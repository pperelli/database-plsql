/*<TOAD_FILE_CHUNK>*/
CREATE OR REPLACE PACKAGE EBT_BT_ZONA_UTIL AS
/******************************************************************************
   NOMBRE:      EBT_BT_ZONA_UTIL

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        26/09/2018  pperelli         Created this package.
******************************************************************************/

    FUNCTION getZonaName(pZonaId IN NUMBER) RETURN VARCHAR2;
    
    FUNCTION getSubZonaName(pSubZonaId IN NUMBER) RETURN VARCHAR2;

    FUNCTION getObjName(pObjId IN NUMBER) RETURN VARCHAR2;

    FUNCTION getCheckPointName(pCheckPointId IN NUMBER) RETURN VARCHAR2;
    
END EBT_BT_ZONA_UTIL;
/

/*<TOAD_FILE_CHUNK>*/
CREATE OR REPLACE PACKAGE BODY EBT_BT_ZONA_UTIL AS

FUNCTION getZonaName(pZonaId IN NUMBER) RETURN VARCHAR2
IS
    sZonaNombre EBA_BT_ZONA.PRODUCT_NAME%TYPE;
BEGIN
    SELECT
    PRODUCT_NAME
    INTO
    sZonaNombre
    FROM
    EBA_BT_ZONA
    WHERE
    ID = pZonaId;
    
    RETURN sZonaNombre;
EXCEPTION
    WHEN OTHERS THEN
        RETURN('');
END;
    
FUNCTION getSubZonaName(pSubZonaId IN NUMBER) RETURN VARCHAR2
IS
    sSubZonaNombre EBA_BT_SUBZONA.NAME%TYPE;
BEGIN
    SELECT
    NAME AS NOMBRE
    INTO
    sSubZonaNombre
    FROM
    EBA_BT_SUBZONA
    WHERE
    ID = pSubZonaId;
    
    RETURN sSubZonaNombre;
EXCEPTION
    WHEN OTHERS THEN
        RETURN('');
END;

FUNCTION getObjName(pObjId IN NUMBER) RETURN VARCHAR2
IS
    sObjNombre EBA_BT_ZONA.PRODUCT_NAME%TYPE;
BEGIN
    SELECT
    NAME
    INTO
    sObjNombre
    FROM
    EBA_BT_OBJ
    WHERE
    ID = pObjId;
    
    RETURN sObjNombre;
EXCEPTION
    WHEN OTHERS THEN
        RETURN('');
END;

FUNCTION getCheckPointName(pCheckPointId IN NUMBER) RETURN VARCHAR2
IS
    sCheckPointNombre EBA_BT_CHECKPOINTS.NAME%TYPE;
BEGIN
    SELECT
    NAME
    INTO
    sCheckPointNombre
    FROM
    EBA_BT_CHECKPOINTS
    WHERE
    ID = pCheckPointId;
    
    RETURN sCheckPointNombre;
EXCEPTION
    WHEN OTHERS THEN
        RETURN('');
END;
 
END EBT_BT_ZONA_UTIL;
/

