/*<TOAD_FILE_CHUNK>*/
CREATE OR REPLACE PACKAGE EBA_BT_FW as
    function conv_txt_html (
        p_txt_message in varchar2 )
        return varchar2;
    function conv_urls_links (
        p_string in varchar2 )
        return varchar2;
    function tags_cleaner (
        p_tags  in varchar2,
        p_case  in varchar2 default 'U' )
        return varchar2;
    procedure tag_sync (
        p_new_tags          in varchar2,
        p_old_tags          in varchar2,
        p_content_type      in varchar2,
        p_content_id        in number );
    function selective_escape (
        p_text  in varchar2,
        p_tags  in varchar2 default '<h2>,</h2>,<p>,</p>,<b>,</b>,<li>,</li>,<ul>,</ul>,<br />,<i>,</i>,<h3>,</h3>' )
        return varchar2;
    function get_preference_value (
        p_preference_name in varchar2 )
        return varchar2;
    procedure set_preference_value (
        p_preference_name  in varchar2, 
        p_preference_value in varchar2 );
    function compress_int (
        n in integer )
        return varchar2;
    function apex_error_handling (
        p_error in apex_error.t_error )
        return apex_error.t_error_result;
        
    function store_img(
        img in blob
    )
    return varchar2;
    
    function storeDoc(doc in varchar2)
    return varchar2;
    
    function sendNotification(
    p_token varchar2,
    p_subject varchar,
    p_body varchar)
    return varchar2;
    
    /**************************************************************************************/
    /*****  25/9/2018 - P.Perelli - insert cabecera y lineas de lecturas checkPoints  *****/
    /**************************************************************************************/
    FUNCTION createLecChkPts( pFechaLec TIMESTAMP,
                              pDelegacionID NUMBER,
                              pCheckPointDefID NUMBER,
                              pOperario VARCHAR2 ) RETURN NUMBER;

    Procedure createAPPCC(
        p_dia           Timestamp,
        p_delegacion    Number,
        p_tipolectura   Number,
        p_tarea         Number default null,
        p_operario      Number,
        p_IsMaquina     Varchar2 default 'N');
  
  Function validateAPPCC(
    p_tipolectura   Number)
  Return VARCHAR2;
    
end eba_bt_fw;
/

/*<TOAD_FILE_CHUNK>*/
CREATE OR REPLACE PACKAGE BODY EBA_BT_FW as
    function conv_txt_html (
        p_txt_message in varchar2 )
        return varchar2
    is
        l_html_message   varchar2(32767) default p_txt_message;
        l_temp_url varchar2(32767) := null;
        l_length number;
    begin
        l_html_message := replace(l_html_message, chr(10), '<br />');
        l_html_message := replace(l_html_message, chr(13), null);
        return l_html_message;
    end conv_txt_html;
    function conv_urls_links (
        p_string in varchar2 )
        return varchar2
    is
        l_string   varchar2(32767) default p_string;
        l_endofUrl varchar2(4000) default chr(10) || chr(13) || chr(9) || ' )<>';
        l_url         varchar2(4000);
        l_current_pos number := 1;
        n             number := 1;
        m             number := 1;
        p             number := 1;
    begin
        l_string := p_string || ' ';
        for i in 1 .. 1000 loop
            n := instr( lower(l_string), 'http://', l_current_pos );
            m := instr( lower(l_string), 'https://', l_current_pos );
            p := instr( lower(l_string), 'ftp://', l_current_pos   );
            -- set n to position of first link
            if m > 0 and (n = 0 or m < n) and (p = 0 or m < p) then
               n := m;
            elsif p > 0 and (n = 0 or p < n) then
               n := p;
            end if;
            exit when n = 0 or length(l_string) > 32000;
            for j in 0 .. length( l_string ) - n loop
                if ( instr( l_endofUrl, substr( l_string, n+j, 1 ) ) > 0 ) then
                   l_url := rtrim( substr( l_string, n, j ), '.'||chr(32)||chr(10) );
                   l_url := '<a href="' || l_url || '">' || l_url || '</a>';
                   l_string := substr( l_string, 1, n-1 ) || l_url || substr( l_string, n+j );
                   l_current_pos := n + length(l_url);
                   exit;
                end if;
            end loop;
        end loop;
        return l_string;
    end conv_urls_links;
    function tags_cleaner (
        p_tags  in varchar2,
        p_case  in varchar2 default 'U' )
        return varchar2
    is
        type tags is table of varchar2(255) index by varchar2(255);
        l_tags_a        tags;
        l_tag           varchar2(255);
        l_tags          apex_application_global.vc_arr2;
        l_tags_string   varchar2(32767);
        i               integer;
    begin
        l_tags := apex_util.string_to_table(p_tags,',');
        for i in 1..l_tags.count loop
            --remove all whitespace, including tabs, spaces, line feeds and carraige returns with a single space
            l_tag := substr(trim(regexp_replace(l_tags(i),'[[:space:]]{1,}',' ')),1,255);
            if l_tag is not null and l_tag != ' ' then
                if p_case = 'U' then
                    l_tag := upper(l_tag);
                elsif p_case = 'L' then
                    l_tag := lower(l_tag);
                end if;
                --add it to the associative array, if it is a duplicate, it will just be replaced
                l_tags_a(l_tag) := l_tag;
            end if;
        end loop;
        l_tag := null;
        l_tag := l_tags_a.first;
        while l_tag is not null loop
            l_tags_string := l_tags_string||l_tag;
            if l_tag != l_tags_a.last then
                l_tags_string := l_tags_string || ', ';
            end if;
            l_tag := l_tags_a.next(l_tag);
        end loop;
        return substr(l_tags_string, 1, 4000);
    end tags_cleaner;
    procedure tag_sync (
        p_new_tags          in varchar2,
        p_old_tags          in varchar2,
        p_content_type      in varchar2,
        p_content_id        in number )
    as
        type tags is table of varchar2(255) index by varchar2(255);
        l_new_tags_a    tags;
        l_old_tags_a    tags;
        l_new_tags      apex_application_global.vc_arr2;
        l_old_tags      apex_application_global.vc_arr2;
        l_merge_tags    apex_application_global.vc_arr2;
        l_dummy_tag     varchar2(255);
        i               integer;
    begin
        l_old_tags := apex_util.string_to_table(p_old_tags,', ');
        l_new_tags := apex_util.string_to_table(p_new_tags,', ');
        if l_old_tags.count > 0 then --do inserts and deletes
            --build the associative arrays
            for i in 1..l_old_tags.count loop
                l_old_tags_a(l_old_tags(i)) := l_old_tags(i);
            end loop;
            for i in 1..l_new_tags.count loop
                l_new_tags_a(l_new_tags(i)) := l_new_tags(i);
            end loop;
            --do the inserts
            for i in 1..l_new_tags.count loop
                begin
                    l_dummy_tag := l_old_tags_a(l_new_tags(i));
                exception when no_data_found then
                    insert into eba_bt_tags (tag, content_id, content_type )
                    values (l_new_tags(i), p_content_id, p_content_type );
                    l_merge_tags(l_merge_tags.count + 1) := l_new_tags(i);
                end;
            end loop;
            --do the deletes
            for i in 1..l_old_tags.count loop
                begin
                    l_dummy_tag := l_new_tags_a(l_old_tags(i));
                exception when no_data_found then
                    delete from eba_bt_tags where content_id = p_content_id and tag = l_old_tags(i);
                    l_merge_tags(l_merge_tags.count + 1) := l_old_tags(i);
                end;
            end loop;
        else --just do inserts
            for i in 1..l_new_tags.count loop
                insert into eba_bt_tags (tag, content_id, content_type )
                values (l_new_tags(i), p_content_id, p_content_type );
                l_merge_tags(l_merge_tags.count + 1) := l_new_tags(i);
            end loop;
        end if;
        for i in 1..l_merge_tags.count loop
            merge into eba_bt_tags_type_sum s
            using (select count(*) tag_count
                     from eba_bt_tags
                    where tag = l_merge_tags(i) and content_type = p_content_type ) t
            on (s.tag = l_merge_tags(i) and s.content_type = p_content_type )
            when not matched then insert (tag, content_type, tag_count)
                                  values (l_merge_tags(i), p_content_type, t.tag_count)
            when matched then update set s.tag_count = t.tag_count;
            merge into eba_bt_tags_sum s
            using (select sum(tag_count) tag_count
                     from eba_bt_tags_type_sum
                    where tag = l_merge_tags(i) ) t
            on (s.tag = l_merge_tags(i) )
            when not matched then insert (tag, tag_count)
                                  values (l_merge_tags(i), t.tag_count)
            when matched then update set s.tag_count = t.tag_count;
        end loop;
    end tag_sync;
    function selective_escape (
        p_text  in varchar2,
        p_tags  in varchar2 default '<h2>,</h2>,<p>,</p>,<b>,</b>,<li>,</li>,<ul>,</ul>,<br />,<i>,</i>,<h3>,</h3>'
        ) return varchar2
    is
        t apex_application_global.vc_arr2;
        x varchar2(32767) := p_text;
    begin
        t := apex_util.string_to_table(p_tags, ',');
        for i in 1..t.count loop
            x := replace(x,t(i),'Aa'||i||'aA');
        end loop;
        x := apex_escape.html(x);
        for i in 1..t.count loop
            x := replace(x,'Aa'||i||'aA',t(i));
        end loop;
        return x;
    end selective_escape;
    function get_preference_value (
        p_preference_name varchar2 )
        return varchar2
    is
        l_preference_value varchar2(255);
    begin
        select preference_value
            into l_preference_value
        from eba_bt_preferences
        where preference_name = p_preference_name;
        return l_preference_value;
    exception
        when no_data_found then
            return 'Preference does not exist';
    end get_preference_value;
    procedure set_preference_value (
        p_preference_name  varchar2, 
        p_preference_value varchar2 )
    is
    begin
        merge into eba_bt_preferences dest
        using ( select upper(p_preference_name) preference_name,
                    p_preference_value preference_value
                from dual ) src
        on ( upper(dest.preference_name) = src.preference_name )
        when matched then
            update set dest.preference_value = src.preference_value
        when not matched then
            insert (dest.preference_name, dest.preference_value)
            values (src.preference_name, src.preference_value);
    end set_preference_value;
    function compress_int (
        n in integer )
        return varchar2
    as
        ret varchar2(30);
        quotient integer;
        remainder integer;
        digit char(1);
    begin
        ret := '';
        quotient := n;
        while quotient > 0
        loop
            remainder := mod(quotient, 10 + 26);
            quotient := floor(quotient  / (10 + 26));
            if remainder < 26 then
                digit := chr(ascii('A') + remainder);
            else
                digit := chr(ascii('0') + remainder - 26);
            end if;
            ret := digit || ret;
        end loop ;
        if length(ret) < 5 then
            ret := lpad(ret, 4, 'A');
        end if ;
        return upper(ret);
    end compress_int;
    procedure add_error_log ( 
        p_error in apex_error.t_error )
    is
    pragma autonomous_transaction;
    begin
        -- Remove old errors
        delete from eba_bt_errors where err_time <= localtimestamp - 21;
        -- Log the error.
        insert into eba_bt_errors (
            app_id,
            app_page_id,
            app_user,
            user_agent,
            ip_address,
            ip_address2,
            message,
            page_item_name,
            region_id,
            column_alias,
            row_num,
            apex_error_code,
            ora_sqlcode,
            ora_sqlerrm,
            error_backtrace )
        select v('APP_ID'),
            v('APP_PAGE_ID'),
            v('APP_USER'),
            owa_util.get_cgi_env('HTTP_USER_AGENT'),
            owa_util.get_cgi_env('REMOTE_ADDR'),
            sys_context('USERENV', 'IP_ADDRESS'),
            substr(p_error.message,0,4000),
            p_error.page_item_name,
            p_error.region_id,
            p_error.column_alias,
            p_error.row_num,
            p_error.apex_error_code,
            p_error.ora_sqlcode,
            substr(p_error.ora_sqlerrm,0,4000),
            substr(p_error.error_backtrace,0,4000)
        from dual;
        commit;
    end add_error_log;
    function apex_error_handling (
        p_error in apex_error.t_error )
        return apex_error.t_error_result
    is
        l_result          apex_error.t_error_result;
        l_constraint_name varchar2(255);
    begin
        l_result := apex_error.init_error_result (
                        p_error => p_error );
        -- If it is an internal error raised by APEX, like an invalid statement or
        -- code which can not be executed, the error text might contain security sensitive
        -- information. To avoid this security problem we can rewrite the error to
        -- a generic error message and log the original error message for further
        -- investigation by the help desk.
        if p_error.is_internal_error then
            -- mask all errors that are not common runtime errors (Access Denied
            -- errors raised by application / page authorization and all errors
            -- regarding session and session state)
            if not p_error.is_common_runtime_error then
                add_error_log( p_error );
                -- Change the message to the generic error message which doesn't expose
                -- any sensitive information.
                l_result.message         := 'An unexpected internal application error has occurred.';
                l_result.additional_info := null;
            end if;
        else
            -- Always show the error as inline error
            -- Note: If you have created manual tabular forms (using the package
            --       apex_item/htmldb_item in the SQL statement) you should still
            --       use "On error page" on that pages to avoid loosing entered data
            l_result.display_location := case
                                           when l_result.display_location = apex_error.c_on_error_page then apex_error.c_inline_in_notification
                                           else l_result.display_location
                                         end;
            -- If it's a constraint violation like
            --
            --   -) ORA-00001: unique constraint violated
            --   -) ORA-02091: transaction rolled back (-> can hide a deferred constraint)
            --   -) ORA-02290: check constraint violated
            --   -) ORA-02291: integrity constraint violated - parent key not found
            --   -) ORA-02292: integrity constraint violated - child record found
            --
            -- we try to get a friendly error message from our constraint lookup configuration.
            -- If we don't find the constraint in our lookup table we fallback to
            -- the original ORA error message.
            if p_error.ora_sqlcode in (-1, -2091, -2290, -2291, -2292) then
                l_constraint_name := apex_error.extract_constraint_name (
                                         p_error => p_error );
                begin
                    select message
                      into l_result.message
                      from eba_bt_error_lookup
                     where constraint_name = l_constraint_name;
                exception when no_data_found then null; -- not every constraint has to be in our lookup table
                end;
            end if;
            -- If an ORA error has been raised, for example a raise_application_error(-20xxx, '...')
            -- in a table trigger or in a PL/SQL package called by a process and we
            -- haven't found the error in our lookup table, then we just want to see
            -- the actual error text and not the full error stack with all the ORA error numbers.
            if p_error.ora_sqlcode is not null and l_result.message = p_error.message then
                l_result.message := apex_error.get_first_ora_error_text (
                                        p_error => p_error );
            end if;
            -- If no associated page item/tabular form column has been set, we can use
            -- apex_error.auto_set_associated_item to automatically guess the affected
            -- error field by examine the ORA error for constraint names or column names.
            if l_result.page_item_name is null and l_result.column_alias is null then
                apex_error.auto_set_associated_item (
                    p_error        => p_error,
                    p_error_result => l_result );
            end if;
        end if;
        return l_result;
    end apex_error_handling;
    
    function store_img(img  blob)
        return varchar2 is
        store_url varchar2(100) := 'http://talatsmarthotel.com/ctrn/index.php';
        v_request    UTL_HTTP.req;
        v_response   UTL_HTTP.resp;
        v_text varchar2(1000);
    
        begin
        
    
        return 'ok';
    
    end store_img;
    
    function sendNotification(
        p_token varchar2,
        p_subject varchar,
        p_body varchar )
    return varchar2 is
     notificacion_url varchar2(100) := 'http://fcm.googleapis.com/fcm/send';
        v_request    UTL_HTTP.req;
        v_response   UTL_HTTP.resp;
        v_text       VARCHAR2(1024);
        v_result varchar2(1024);
  BEGIN
    
    DBMS_OUTPUT.put_line('Begin Send Message: ' || notificacion_url);
  
     v_text := '{"to": "'||p_token|| '","data": { "body" : "'||regexp_replace(p_body, '<.*?>')||'", "title" : "'|| p_subject ||'", "icon" : "talat_notification_icon_solo" },"notification" : { "body" : "'||regexp_replace(p_body, '<.*?>')||'", "title" : "'|| p_subject ||'", "icon" : "talat_notification_icon_solo" }}';

    v_request := UTL_HTTP.begin_request(notificacion_url, 'POST');

    UTL_HTTP.set_header(v_request, 'Content-Type', 'application/json');
    UTL_HTTP.set_header(v_request, 'Accept', 'application/json');
    UTL_HTTP.set_header(v_request, 'Content-length', length(v_text));
    UTL_HTTP.set_header(v_request, 'Authorization', 'key=AAAAumgQ1n0:APA91bHpDerti_2ta_PhdSB4NPQRC3q-YCVYUzwqO3JdaQDz9ngcjapEbCcxbte32qMDwnhmEbMjEdh8WuW915EtxfpPlHiouuUAbMc11yUSvMvcQ1G0jX0fK_ETrUiZQXrWJQp7muRI');
    
    

    UTL_HTTP.write_text(v_request, v_text);

    v_response := UTL_HTTP.get_response(v_request);

    DBMS_OUTPUT.put_line(RPAD('=', 80, '='));
    DBMS_OUTPUT.put_line('Response status code: ' || v_response.status_code);
    DBMS_OUTPUT.put_line('Response reason phrase: ' || v_response.reason_phrase);

    LOOP
        BEGIN
            UTL_HTTP.read_text(v_response, v_text);
            DBMS_OUTPUT.put_line(v_text);
            v_result := v_result || v_text;
        EXCEPTION
            WHEN UTL_HTTP.end_of_body
            THEN
                NULL;
        END;

        EXIT WHEN v_text IS NULL;
    END LOOP;

    DBMS_OUTPUT.put_line(RPAD('=', 80, '='));

    UTL_HTTP.end_response(v_response);
    
    RETURN 'OK';
  end sendNotification;
    
    
    function storeDoc(doc in varchar2)
    return varchar2 is
    
   
	v_req utl_http.req;
	v_resp utl_http.resp;
	v_parts utl_http_multipart.parts := utl_http_multipart.parts();
    Upload_blob BLOB;
    doc_size integer; 
    doc_name varchar2(100);
    l_buffer varchar2(32766);

begin

 select blob_content  
   into   Upload_blob  
   from   APEX_APPLICATION_TEMP_FILES  
   where  name = doc; 
   
    select FileName
   into   doc_name  
   from   APEX_APPLICATION_TEMP_FILES  
   where  name = doc;  
  
   doc_size := dbms_lob.getlength(Upload_blob); 
   
  
	utl_http_multipart.add_file(v_parts, 'userFile', doc_name, 'application/pdf', Upload_blob);
	utl_http_multipart.add_param(v_parts, 'name', 'userFile');
    utl_http_multipart.add_param(v_parts, 'alias', 'userFile');
	v_req := utl_http.begin_request(get_preference_value('DOCSERVER'), 'POST', 'HTTP/1.1');
	utl_http_multipart.send(v_req, v_parts);

    v_resp := utl_http.get_response(v_req);
    
     BEGIN
     
      UTL_HTTP.read_text(v_resp, l_buffer, 32766);
      
  EXCEPTION
    WHEN UTL_HTTP.end_of_body THEN
      UTL_HTTP.end_response(v_resp);
  END;
  
  
	/*if(v_resp.status_code <> UTL_HTTP.HTTP_OK) then
	   null;
	end if;*/
    /*recollir sa resposta*/
    return l_buffer;
end storeDoc;

/**************************************************************************************/
/*****  25/9/2018 - P.Perelli - insert cabecera y lineas de lecturas checkPoints  *****/
/**************************************************************************************/
FUNCTION createLecChkPts( pFechaLec TIMESTAMP,
                          pDelegacionID NUMBER,
                          pCheckPointDefID NUMBER,
                          pOperario VARCHAR2 ) RETURN NUMBER
IS
    sProc VARCHAR2(30):='EBAT_BT_FW.createLecChkPts';
    nIdLecturadiaria EBA_BT_LECTURADIARIA.ID%TYPE;
    bSuccess BOOLEAN:=FALSE;
BEGIN
    BEGIN
        --insert CABECERA defecto into EBA_BT_LECTURADIARIA
        INSERT INTO EBA_BT_LECTURADIARIA (
            DIA,
            DELEGACION_ID,
            OPERARIO,
            BORRADOR
        )
        VALUES (
            pFechaLec,
            pDelegacionID,
            pOperario,
            'Y'
        ) RETURNING ID INTO nIdLecturadiaria;
        
        bSuccess:=TRUE;
    EXCEPTION
        WHEN OTHERS THEN
            APEX_DEBUG.LOG_MESSAGE(sProc||' - EXCEPTION - INSERT EBA_BT_LECTURADIARIA - '||SQLERRM, TRUE, 1);
    END;
    
    APEX_DEBUG.LOG_MESSAGE(sProc||' - INSERT EBA_BT_LECTURADIARIA - LecturaID '||TO_CHAR(nIdLecturadiaria)||' created');
    
    IF bSuccess THEN  --insert detalle SOLO si exito insert cabecera
        BEGIN
            --insert detalle defecto into EBA_BT_LECTURADIARIA_CHK_LIN
            INSERT INTO EBA_BT_LECTURADIARIA_CHK_LIN (
                DELEGACION_ID,
                ZONA_ID,
                SUBZONA_ID,
                CHECKPOINT_DEF_ID,
                CHECKPOINT_ID,
                OBJ_ID,
                LECTURADIARIA_ID,
                VALOR
            )
            SELECT
                pDelegacionID,
                O.ZONA_ID,
                O.SUBZONA_ID,
                pCheckPointDefID,
                OTC.CHECKPOINT_ID,
                O.ID AS OBJ_ID,
                nIdLecturadiaria, --ID cabecera
                DECODE(CP.HAS_VALUE,'Y','','N') AS DEFECTO --si tiene valor (HAS_VALUE = 'Y') por defecto escribe texto vac�o, si NO tiene valor (HAS_VALUE='N') es una lectura de estado Y/N, por defecto escribe 'N'
            FROM
                EBA_BT_OBJ O,
                EBA_BT_O_O_TYPES_CHECKPOINTS OTC,
                EBA_BT_CHECKPOINTS CP
            WHERE
                O.DELEGACION_ID = pDelegacionID
                AND OTC.OBJ_ID = O.ID
                AND CP.ID = OTC.CHECKPOINT_ID;
        EXCEPTION
            WHEN OTHERS THEN
                APEX_DEBUG.LOG_MESSAGE(sProc||' - EXCEPTION - INSERT EBA_BT_LECTURADIARIA_CHK_LIN - '||SQLERRM, TRUE, 1);
                ROLLBACK;
        END;
        
        APEX_DEBUG.LOG_MESSAGE(sProc||' - INSERT EBA_BT_LECTURADIARIA_CHK_LIN - LecturaID '||TO_CHAR(nIdLecturadiaria)||' created');
    END IF;
    
    IF bSuccess THEN
        RETURN nIdLecturadiaria;
    ELSE
        RETURN 0;
    END IF;
    
    COMMIT;
EXCEPTION
    WHEN OTHERS THEN
        APEX_DEBUG.LOG_MESSAGE(sProc||' - EXCEPTION GLOBAL - '||SQLERRM);
END;


/**/
Procedure createAPPCC(
    p_dia           Timestamp,
    p_delegacion    Number,
    p_tipolectura   Number,
    p_tarea         Number default null,
    p_operario      Number,
    p_IsMaquina     Varchar2 default 'N') 
Is
    --
    id_lecturadiaria number;
    cuenta number;
    --
Begin
    --
    If p_IsMaquina = 'N' Then
      --
      Select  Count(1) 
      Into    cuenta 
      From    EBA_BT_LECTURADIARIA LD 
      Where   DELEGACION = p_delegacion 
      And     to_char(DIA,'DD-MM-RRRR') = TO_CHAR(p_dia,'DD-MM-RRRR') 
      And     TIPOLECTURA_ID = p_tipolectura
      --And     not exists ( SELECT 1 FROM EBA_BT_LECTURADIARIA_LINEA WHERE LECTURADIARIA_ID = LD.ID AND OBJ_ID IS NOT NULL AND ROWNUM <= 1)
      And     Rownum <= 1; -- Con que haya uno es bastante ( asi optimizamos la consulta)
      --
    Else
      --
      Select  Count(1) 
      Into    cuenta 
      From    EBA_BT_LECTURADIARIA LD
      Where   DELEGACION = p_delegacion 
      And     to_char(DIA,'DD-MM-RRRR') = TO_CHAR(p_dia,'DD-MM-RRRR')  
      And     TIPOLECTURA_ID = p_tipolectura
      And     exists ( SELECT 1 FROM EBA_BT_LECTURADIARIA_LINEA WHERE LECTURADIARIA_ID = LD.ID AND OBJ_ID IS NOT NULL AND ROWNUM <= 1) 
      And     Rownum <= 1; -- Con que haya uno es bastante ( asi optimizamos la consulta)
      --
    End If;      
    --
    If cuenta > 0 Then
      Null;
    Else 
      /*GENERAL_ACS_MINIM_TEMP,*/
      Insert Into EBA_BT_LECTURADIARIA(
        DIA,
        DELEGACION,
        OPERARIO,
        TIPOLECTURA_ID, 
        EVENT_ID,
        GENERAL_ACS_MINIM_TEMP,
        GENERAL_AFS_MINIM_CL,
        GENERAL_AFS_MAXIM_CL,
        GENERAL_AFS_MINIM_PH,
        GENERAL_AFS_MAXIM_PH,
        GENERAL_AFS_MAXIM_TEMP) 
      Values(
        p_dia,
        p_delegacion,
        p_operario,
        p_tipolectura,
        p_tarea,
        eba_bt_fw.get_preference_value('GENERAL_ACS_MINIM_TEMP'),  
        eba_bt_fw.get_preference_value('GENERAL_AFS_MINIM_CL'), 
        eba_bt_fw.get_preference_value('GENERAL_AFS_MAXIM_CL'),  
        eba_bt_fw.get_preference_value('GENERAL_AFS_MINIM_PH'), 
        eba_bt_fw.get_preference_value('GENERAL_AFS_MAXIM_PH'),    
        eba_bt_fw.get_preference_value('GENERAL_AFS_MAXIM_TEMP')    
      ) Returning ID Into id_lecturadiaria;
    
      If p_IsMaquina = 'N' Then
        --
        Insert  Into EBA_BT_LECTURADIARIA_LINEA(
          LECTURADIARIA_ID,
          DIA,
          ORDEN,
          OBJ_ID,
          ZONA_ID,
          SUBZONA_ID,
          DELEGACION_ID)  
        Select  id_lecturadiaria LECTURADIARIA_ID, 
                p_dia DIA, 
                L.ORDEN, 
                L.OBJ_ID,
                L.ZONA,
                CASE WHEN SUBZONA = 0 THEN  NULL ELSE SUBZONA END SUBZONA_ID,
                DELEGACION_ID
        From    EBA_BT_DEFINICION_LECTURAS L 
        Where   L.DELEGACION_ID = p_delegacion
        And     l.definicion_id = p_tipolectura
        And     (FLAG_AFS_PH = 'Y' OR FLAG_ACS_T = 'Y' OR FLAG_AFS_CL = 'Y' OR FLAG_AFS_T = 'Y' )
        Order   By L.ORDEN;
        --
      Else
        -- 
        Insert  Into EBA_BT_LECTURADIARIA_LINEA(
          LECTURADIARIA_ID,
          DIA,
          ORDEN,
          OBJ_ID,
          ZONA_ID,
          SUBZONA_ID,
          DELEGACION_ID)
        Select  id_lecturadiaria LECTURADIARIA_ID, 
                p_dia DIA, 
                L.ORDEN, 
                L.OBJ_ID,
                L.ZONA,
                CASE WHEN SUBZONA = 0 THEN  NULL ELSE SUBZONA END SUBZONA_ID,
                DELEGACION_ID
        From    EBA_BT_DEFINICION_LECTURAS L 
        Where   L.DELEGACION_ID = p_delegacion
        And     l.definicion_id = p_tipolectura 
        And     l.OBJ_ID > 0
        Order   By L.ORDEN;  
        -- 
      End If;
      --
    END IF;
    /*
    CASE WHEN L.TIPO = 'AGUA' THEN (CASE WHEN L.FLAG_AFS_PH = 'Y' THEN 
                                    apex_item.text(1,null)
                                    ELSE '' END) ELSE '' END AFS_PH  --AFS_PH 
    ,CASE WHEN L.TIPO = 'AGUA' THEN (CASE WHEN L.FLAG_ACS_T = 'Y' THEN 
                                      apex_item.text(2,null) ELSE '' END) ELSE '' END ACS_T  --AFS_PH 
    ,CASE WHEN L.TIPO = 'AGUA' THEN (CASE WHEN L.FLAG_AFS_CL = 'Y' THEN 
                                      apex_item.text(3,null) ELSE '' END) ELSE '' END AFS_CL  --AFS_PH 
    ,CASE WHEN L.TIPO = 'AGUA' THEN (CASE WHEN L.FLAG_AFS_T = 'Y' THEN
                                      apex_item.text(4,null) ELSE '' END) ELSE '' END AFS_T  --AFS_PH 
    ,CASE WHEN L.TIPO = 'CONTADOR' THEN
                                     apex_item.text(5,null) ELSE '' END CONTADOR  --AFS_PH 
    ,NVL(Z.PRODUCT_NAME,'SIN ZONA') ZONA
    ,CASE WHEN NVL(SUBZONA,0) = 0 THEN 
    CASE WHEN L.ZONA IS NOT NULL THEN APEX_ITEM.SELECT_LIST_FROM_QUERY( 6, null,'SELECT NAME,ID FROM EBA_BT_SUBZONA WHERE ZONA_ID = ' || l.zona,null,'NO',p_show_extra=>'NO')   ELSE '-' END ELSE SZ.NAME END SUBZONA
    ,NVL(L.TEXTO_ALTERNATIVO, Z.product_name || sz.name) TEXTO  
    FROM EBA_BT_DEFINICION_LECTURAS L 
    left outer join EBA_BT_PRODUCT Z ON Z.ID = L.ZONA  
    left outer join EBA_BT_SUBZONA SZ ON SZ.ID = L.SUBZONA  
    WHERE L.DELEGACION_ID = :P187_DELEGACION
    ORDER BY L.ORDEN
    */

  End createAPPCC;

  Function validateAPPCC(
    p_tipolectura   Number)
  Return VARCHAR2
  IS
    --
    v_Count NUMBER;
    --
    v_IsError BOOLEAN := FALSE;
    v_Error   VARCHAR2(4000);
    v_ErrSub  VARCHAR2(4000);
    --
    CURSOR c_ErrorSubzonas (c_tipolectura NUMBER) IS
      SELECT  TD.NAME DEFINICION, Z.PRODUCT_NAME ZONA
      FROM    EBA_BT_TIPO_DEFINICION      TD,
              EBA_BT_DEFINICION_LECTURAS  DL,
              EBA_BT_ZONA                 Z
      WHERE   DL.DEFINICION_ID  = TD.ID
      AND     DL.ZONA	          = Z.ID	
      AND     DEFINICION_ID     = c_tipolectura
      AND     SUBZONA           = 0      
      AND     Z.IS_ACTIVE	= 'Y'
      AND     NOT EXISTS (SELECT  1
                          FROM    EBA_BT_ZONA    Z,
                                  EBA_BT_SUBZONA SZ
                          WHERE   Z.ID = SZ.ZONA_ID
                          AND     Z.ID = DL.ZONA                          
                          AND     SZ.IS_ACTIVE = 'Y'
                          AND     ROWNUM = 1);
    --
  BEGIN
    --
    /* COMPROBAMOS SI LA TODAS LAS DEFINICIONES Y SUS ZONAS TIENEN SUBZONAS ELEGIBLES. */
    --
    FOR r_ErrSubzona IN c_ErrorSubzonas(p_tipolectura) LOOP
      --
      v_IsError := TRUE;
      IF v_Error IS NULL THEN
        v_ErrSub := '&nbsp;&nbsp;* Error de Subzona:</br>';        
      END IF;
      --
      v_ErrSub := v_ErrSub || '&nbsp;&nbsp;** La zona <b>' || r_ErrSubzona.ZONA || '</b> de la definici�n <b>' || r_ErrSubzona.DEFINICION || '</b> no tiene subzonas configuradas.</br>'; 
      --
    END LOOP; 
    --
    /* RECOPILAMOS LOS ERRORES ENCONTRADOS */
    --
    IF v_IsError THEN
      v_Error := ' - <b><u>Revise las definiciones de lecturas.</u></b> - </br>' ;
    END IF;
    
    IF v_ErrSub IS NOT NULL THEN
      v_Error := v_Error || v_ErrSub; 
    END IF;
    --
    RETURN v_Error;
    --
  END;


end eba_bt_fw;
/

